"""
Author: Dawid Dec

Callbacks scheduler for tornado. Can scheduler time to be run on
specified datetime (maximum one second granulation) or after specified
period of time.
"""
# base imports
import time
import datetime

# 3rd party imports
import tornado
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.gen import coroutine

class Scheduler(object):
    def __init__(self):
        self.ioloop = IOLoop.instance()
        self.__datetime_jobs = []
        self.__periodic_jobs = []
        self.__set__up()

    @coroutine
    def __set__up(self):
        PeriodicCallback(self.__check_jobs, 1000).start()

    @coroutine
    def __check_jobs(self):
        now = datetime.datetime.now()

        now = datetime.datetime(year=now.year,
                                month=now.month,
                                day=now.day,
                                hour=now.hour,
                                minute=now.minute,
                                second=now.second,
                                microsecond=0)

        for job in self.__datetime_jobs:
            job_datetime = datetime.datetime(year=now.year,
                                             month=now.month,
                                             day=job.day,
                                             hour=job.hour,
                                             minute=job.minute,
                                             second=job.second,
                                             microsecond=0)

            if job_datetime == now:
                self.ioloop.add_callback(job.job)


    def schedule_datetime(self, day, hour, minute, second, job):
        """
        Schedules job to be run on specified datetime.

        :param day: day for callback to be run
        :param hour: hour for callback to be run
        :param minute: minute for callback to be run
        :param second: second for callback to be run
        :param job: function to be run as callback, should be coroutine.
                    If function is with arguments, this parameter should
                    get functools.partial() object with appended
                    arguments to function call.
        """
        job = ScheduledJob(day=day,
                           hour=hour,
                           minute=minute,
                           second=second,
                           job=job)

        return self.__datetime_jobs.append(job)

    def schedule_periodic(self, job, days=None, hours=None, minutes=None, seconds=None):
        """
        Schedules periodic callback. You can delay using days, hours,
        minutes or seconds.
        """
        args = locals()
        args.pop('self')

        job = args.pop('job')

        check = set(args.values())
        check.remove(None)

        if len(check) > 1:
            print "Sorry but you can use only one argument as delay param. \
                   Choose between days, hours, minutes or seconds."

        if days:
            delay = days * 24 * 60 * 60 * 1000
        elif hours:
            delay = hours * 60 * 60 * 1000
        elif minutes:
            delay = minutes * 60 * 1000
        elif seconds:
            delay = seconds * 1000
        else:
            raise AttributeError('Didn\'t provide valid value!')

        try:
            name = job.__name__
        except AttributeError:
            name = job.func.__name__

        print "Started periodic callback. Job: {job}, Period: {period} ms"\
                .format(job=name, period=delay)

        periodic = PeriodicCallback(job, delay)

        periodic.start()

        return periodic

    def schedule_delayed(self, job, days=None, hours=None, minutes=None, seconds=None):
        """
        Schedules delayed callback. You can delay using days, hours,
        minutes or seconds.
        """

        args = locals()
        args.pop('self')
        job = args.pop('job')
        check = set(args.values())
        check.remove(None)

        if len(check) > 1:
            print "Sorry but you can use only one argument as delay param. \
                   Choose between days, hours, minutes or seconds."

        if days:
            delay = days * 24 * 60 * 60
        elif hours:
            delay = hours * 60 * 60
        elif minutes:
            delay = minutes * 60
        elif seconds:
            delay = seconds
        else:
            raise AttributeError('Didn\'t provide valid value!')

        print("Delaying: " + str(delay))
        self.ioloop.add_timeout(time.time() + delay, job)
        return

class ScheduledJob(object):
    def __init__(self, day=None, hour=None, minute=None, second=None, job=None):
        self.__day = day
        self.__hour = hour
        self.__minute = minute
        self.__second = second
        self.__job = job
        self.ioloop = IOLoop.instance()

    def __getattr__(self, attr):
        priv_attr = "_ScheduledJob__{}".format(attr)
        if priv_attr in self.__dict__:
            return self.__dict__[priv_attr]
        return super(ScheduledJob, self).__getattribute__(attr)


