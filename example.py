import scheduler


def foo(i):
	print i

f = partial(func, 1)

scheduler.Scheduler().schedule_periodic(
           f,
           seconds=3
)

scheduler.Scheduler().schedule_delayed(
           f,
           seconds=1
)

scheduler.Scheduler().schedule_datetime(
	   day=10,
	   hour=1,
	   minute=10,
	   second=1,
           f,
)


